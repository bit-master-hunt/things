var User = require('./userModel');

function init( cb ) {
   var saved = 0;
   var result = [];
   const userDb = [["bilbo", "baggins"], ["frodo", "baggins"], ["samwise", "gamgee"], ["gandalf", "gray"]].forEach( names => {
           var user = new User( {
                   name : { first : names[0], last : names[1] },
                   password : "123",
                   email : names[0] + "@mordor.org",
                   enabled : true
               } );

           user.save( function(err, savedUser ) {
                   result.push( savedUser );
                   if( result.length == 4 ) cb( null, result );
               } );
   } );
};
module.exports.init = init;

/*
 * userDb is an object having
 * user.email as keys
 * and user objects as values
 */
function save( user, cb ) {
   new User(user).save( cb );
}
module.exports.save = save;

function findAll( cb ) {
    User.find( {}, cb );
}
module.exports.findAll = findAll;

function findById( id, cb ) {
    User.findOne( { '_id' : id }, cb );
}
module.exports.findById = findById;

function findByEmail( email, cb ) {
    User.findOne( { 'email' : email }, cb );
}
module.exports.findByEmail = findByEmail;
