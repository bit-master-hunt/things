var express = require('express');
var router = express.Router();
var users = require('./users');
var things = require('./things');

function contains( text, key ) {
   return text.toUpperCase().indexOf( key.toUpperCase() ) >= 0;
};

/**************** ROUTES ***********************************/
router.all('*', function(req, res, next) {
        console.log(req.method, req.path);
        next();
} );

// put this one first to avoid authentication concerns
router.get('/users/init', function( req, res, next ) {
    users.init( (err, result) => res.json( result ) );
} );

router.all( '/users/:uid/*', function( req, res, next ) {
   users.findById( req.params.uid, function( error, pathUser ) {
      var authenticatedUser = req.session.user;
      if( authenticatedUser && pathUser && authenticatedUser.id == pathUser.id ) {
         next();
      } else {
         res.redirect( '/' );
      }
   } );
} );

router.get('/users', function( req, res, next  ) {
   users.findAll( function(error, result) {
      if( error ) {
         res.status(500).json( error);
      } else {
         res.json( result );
      }
   } );
} );

router.get('/users/:uid/things', function( req, res, next ) {
   var filter = req.query.filter || '';
   things.findByOwner( req.params.uid, filter, function( err, result ) {
      if( err ) {
         res.json( { msg : err } );
      } else {
         res.json( result );
      }
   } );
} );

router.get('/users/:uid/things/:tid', function( req, res, next ) {
   things.find( req.params.uid, req.params.tid, function( err, thing ) {
      if( thing ) {
         res.json( result );
      } else {
         res.status( 404 ).send( 'no such thing' );
      }
   } );
} );
           
router.post('/users/:uid/things', function( req, res, next ) {
   things.create( req.params.uid, req.body.name, req.body.value, function( err, thing ) {
      if( err ) {
         res.status( 500 ).send( { 'msg' : 'Error creating thing' } );
      } else {
         res.send( thing );
      }
   } );
} );

router.delete('/users/:uid/things/:tid', function( req, res, next ) {
   things.deleteThing( req.params.uid, req.params.tid, function( err, result ) {
      if( err ) {
         res.status( 400 ).send( { msg : err } );
      } else {
         res.json( result );
      }
   } );
} );

router.put('/users/:uid/things/:tid', function( req, res, next ) {
   things.update( req.params.uid, req.params.tid, req.body, function( err, thing ) {
      if( err ) {
         res.status( 403 ).json( { msg : err } );
      } else {
         res.json( thing );
      }
   } );
} );

module.exports = router;
